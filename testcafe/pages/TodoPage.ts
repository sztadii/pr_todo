import Page from './Page'

export default class TodoPage extends Page {
  private addInput;
  private item;
  private itemClose;
  private reset;
  private message;

  constructor () {
    super()
    this.addInput = this.selector('.Input')
    this.item = this.selector('.TodoItem')
    this.itemClose = this.selector('.TodoItem__close')
    this.reset = this.selector('.TodoReset')
    this.message = this.selector('.Message')
  }

  async addNewTodo () {
    const itemCount = await this.item.count

    await this.driver
      .typeText(this.addInput, 'Some message')
      .pressKey('enter')
      .expect(this.addInput.value)
      .eql('')
      .expect(this.item.count)
      .eql(itemCount + 1)
  }

  async removeTodo () {
    await this.driver
      .hover(this.item.nth(0))
      .setNativeDialogHandler(() => true)
      .click(this.itemClose)
      .expect(this.item.count)
      .eql(0)
  }

  async resetTodo () {
    await this.driver
      .setNativeDialogHandler(() => true)
      .click(this.reset)
      .expect(this.item.count)
      .eql(0)
  }

  async triggerError () {
    await this.driver
      .typeText(this.addInput, 'x')
      .pressKey('enter')
      .expect(this.addInput.value)
      .eql('x')
      .expect(this.message.count)
      .eql(1)
      .typeText(this.addInput, 'xxxxxx')
      .expect(this.message.count)
      .eql(0)
      .pressKey('enter')
      .expect(this.message.count)
      .eql(0)
  }

  async typeText (text: string) {
    await this.driver.typeText(this.addInput, text)
  }

  async checkTextFieldIsFilled (text: string) {
    await this.driver.expect(this.addInput.value).eql(text)
  }
}
