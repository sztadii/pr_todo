import Page from './Page'

export default class NoMatchPage extends Page {
  private page;
  private link;

  constructor () {
    super()
    this.page = this.selector('.NoMatch')
    this.link = this.selector('.Back a')
  }

  async checkView () {
    await this.driver.expect(this.page.count).eql(1)
  }

  async clickLink () {
    await this.driver.click(this.link)
  }
}
