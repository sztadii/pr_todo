import Page from './Page'

export default class StatsPage extends Page {
  private chart;

  constructor () {
    super()
    this.chart = this.selector('.Stats__chart')
  }

  async checkChart () {
    await this.driver.expect(this.chart.count).eql(1)
  }
}
