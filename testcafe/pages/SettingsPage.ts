import Page from './Page'

export default class SettingsPage extends Page {
  private langName;
  private select;
  private langPl;
  private reset;
  private color;

  constructor () {
    super()
    this.langName = this.selector('.SettingsLang__name')
    this.select = this.selector('.SettingsLang__select')
    this.langPl = this.select
      .find('option')
      .filter(option => option.getAttribute('value') === 'pl')
    this.reset = this.selector('.SettingsReset')
    this.color = this.selector('.SettingsColor')
  }

  async changeLang () {
    await this.driver
      .click(this.select)
      .click(this.langPl)
      .expect(this.langName.textContent)
      .eql('Wybierz język')
  }

  async resetToDefaults () {
    await this.driver
      .setNativeDialogHandler(() => true)
      .click(this.reset)
      .expect(this.langName.textContent)
      .eql('Choose lang')
  }
}
