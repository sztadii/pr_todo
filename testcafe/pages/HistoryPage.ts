import Page from './Page'

export default class HistoryPage extends Page {
  private item;

  constructor () {
    super()
    this.item = this.selector('.HistoryItem')
  }

  async checkNewHistoryItems () {
    await this.driver.expect(this.item.count).eql(2)
  }
}
