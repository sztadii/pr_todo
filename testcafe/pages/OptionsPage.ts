import Page from './Page'

export default class OptionsPage extends Page {
  private activeButton: string;
  private active;
  private effectActive;

  constructor () {
    super()
    this.activeButton = 'esc'
    this.active = this.selector('.Options--active')
    this.effectActive = this.selector('.OptionsEffect--active')
  }

  async show () {
    await this.driver.pressKey(this.activeButton)
  }

  async checkToggler () {
    await this.show()

    await this.driver
      .expect(this.active.count)
      .eql(1)
      .expect(this.effectActive.count)
      .eql(1)

    await this.show()

    await this.driver
      .expect(this.active.count)
      .eql(0)
      .expect(this.effectActive.count)
      .eql(0)
  }
}
