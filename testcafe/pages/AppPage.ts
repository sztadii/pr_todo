import routes from '../../src/config/routes'
import Page from './Page'

export default class AppPage extends Page {
  async visitTodo () {
    await this.driver.navigateTo(routes.baseUrl + routes.todo)
  }

  async visitHistory () {
    await this.driver.navigateTo(routes.baseUrl + routes.history)
  }

  async visitStats () {
    await this.driver.navigateTo(routes.baseUrl + routes.stats)
  }

  async visitUnknown () {
    await this.driver.navigateTo(`${routes.baseUrl}xxxxxxxx`)
  }
}
