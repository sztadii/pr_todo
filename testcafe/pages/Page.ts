import { Selector, t } from 'testcafe'

export default class Page {
  protected driver: TestController
  protected selector: Selector

  constructor() {
    this.driver = t;
    this.selector = Selector.bind(this)
  }
}
