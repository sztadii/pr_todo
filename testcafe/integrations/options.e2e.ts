import routes from '../../src/config/routes'
import OptionsPage from '../pages/OptionsPage'

fixture('Options page')
  .page(routes.baseUrl)

const options = new OptionsPage()

test('Options page working properly', async () => {
  await options.checkToggler()
})
