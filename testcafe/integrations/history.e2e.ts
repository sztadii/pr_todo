import routes from '../../src/config/routes'
import HistoryPage from '../pages/HistoryPage'
import TodoPage from '../pages/TodoPage'
import AppPage from '../pages/AppPage'

fixture('History page')
  .page(routes.baseUrl)

const history = new HistoryPage()
const todo = new TodoPage()
const app = new AppPage()

test('Add history items working properly', async () => {
  await todo.addNewTodo()
  await todo.addNewTodo()
  await app.visitHistory()
  await history.checkNewHistoryItems()
})
