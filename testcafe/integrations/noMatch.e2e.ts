import routes from '../../src/config/routes'
import NoMatchPage from '../pages/NoMatchPage'
import AppPage from '../pages/AppPage'
import TodoPage from '../pages/TodoPage'

fixture('NoMatch page')
  .page(routes.baseUrl)

const noMatch = new NoMatchPage()
const app = new AppPage()
const todo = new TodoPage()

test('NoMatch page shows when page not exists', async () => {
  await app.visitUnknown()
  await noMatch.checkView()
})

test('Link on NoMatch page redirect to Todo page', async () => {
  await app.visitUnknown()
  await noMatch.clickLink()
  await todo.addNewTodo()
})
