import routes from '../../src/config/routes'
import StatsPage from '../pages/StatsPage'
import TodoPage from '../pages/TodoPage'
import AppPage from '../pages/AppPage'

fixture('Stats page')
  .page(routes.baseUrl)

const stats = new StatsPage()
const todo = new TodoPage()
const app = new AppPage()

test('Generate stats working correctly', async () => {
  await todo.addNewTodo()
  await app.visitStats()
  await stats.checkChart()
})
