import routes from '../../src/config/routes'
import TodoPage from '../pages/TodoPage'
import AppPage from '../pages/AppPage'

fixture('Todo page')
  .page(routes.baseUrl)

const todo = new TodoPage()
const app = new AppPage()

test('Adding new todo working properly', async () => {
  await todo.addNewTodo()
})

test('Remove todo item working properly', async () => {
  await todo.addNewTodo()
  await todo.removeTodo()
})

test('Reset todos working properly', async () => {
  await todo.addNewTodo()
  await todo.resetTodo()
})

test('Error message working properly', async () => {
  await todo.triggerError()
})

test('Text is saving to storage properly', async () => {
  await todo.typeText('Some text')
  await app.visitHistory()
  await app.visitTodo()
  await todo.checkTextFieldIsFilled('Some text')
})
