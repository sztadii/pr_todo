import routes from '../../src/config/routes'
import SettingsPage from '../pages/SettingsPage'
import OptionsPage from '../pages/OptionsPage'

fixture('Settings page')
  .page(routes.baseUrl)

const settings = new SettingsPage()
const options = new OptionsPage()

test('On settings page we can change language of app', async () => {
  await options.show()
  await settings.changeLang()
})

test('On settings page we can reset settings to defaults', async () => {
  await options.show()
  await settings.changeLang()
  await settings.resetToDefaults()
})
