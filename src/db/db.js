class Database {
  constructor (objects) {
    this.objects = objects
  }

  get = object => JSON.parse(localStorage.getItem(object))

  set = (object, value) => {
    this._setLocal(object, value)
    return value
  }

  create = (value, time = new Date().getTime()) => ({
    id: time,
    ...value
  })

  push = (object, value) => {
    const create = this.create(value)
    this._pushLocal(object, create)
    this._pushLocal(`history-${object}`, create)
    return create
  }

  remove = (object, id) => {
    const oldElement = this.get(object)
    const newElement = oldElement.filter(item => item.id !== id)
    this._setLocal(object, newElement)
    return id
  }

  reset = () => {
    Object.values(this.objects).forEach(key => {
      localStorage.removeItem(key)
    })
  }

  resetObject = object => {
    localStorage.removeItem(object)
  }

  _setLocal (object, value) {
    localStorage.setItem(object, JSON.stringify(value))
  }

  _pushLocal (object, value) {
    const oldElement = this.get(object)
    const newElement = oldElement ? [value, ...oldElement] : [value]
    this._setLocal(object, newElement)
  }
}

const objects = {
  list: 'list',
  settings: 'settings',
  text: 'text'
}

const db = new Database(objects)

export default db
