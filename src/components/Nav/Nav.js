import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import MainLink from '../MainLink/MainLink'
import links from './links'
import translations from './translations'
import connector from './NavConnector'

class Nav extends PureComponent {
  renderLinks = array =>
    array.map(item => {
      const name = translations[item.key][this.props.lang]
      return (
        <MainLink
          path={item.path}
          name={name}
          isGroup
          isPreview
          key={item.path}
          prevX='right'
          prevY='up'
        />
      )
    })

  render () {
    return <div className='Nav'>{this.renderLinks(links)}</div>
  }
}

Nav.propTypes = {
  lang: PropTypes.string.isRequired
}

export default connector(Nav)
