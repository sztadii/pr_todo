export default {
  home: {
    en: 'Home',
    pl: 'Główna',
    de: 'Zuhause',
    es: 'índice'
  },
  history: {
    en: 'History',
    pl: 'Historia',
    de: 'Geschichte',
    es: 'Historia'
  },
  stats: {
    en: 'Statistics',
    pl: 'Statystyki',
    de: 'Statistiken',
    es: 'Estadística'
  }
}
