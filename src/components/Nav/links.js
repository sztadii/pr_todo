export default [
  {
    key: 'home',
    path: '/'
  },
  {
    key: 'history',
    path: '/history'
  },
  {
    key: 'stats',
    path: '/stats'
  }
]
