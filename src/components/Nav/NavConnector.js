import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { settingsLang } from 'src/store/main/selectors'

function mapStateToProps (state) {
  return {
    lang: settingsLang(state)
  }
}

export default compose(connect(mapStateToProps))
