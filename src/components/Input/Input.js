import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

export default class Input extends PureComponent {
  handleChange = e => {
    this.props.onChange(e.target.value)
  }

  render () {
    const { value, placeholder, maxLength } = this.props

    return (
      <input
        onInput={this.handleChange}
        value={value}
        placeholder={placeholder}
        maxLength={maxLength}
        className='Input'
        type='text'
      />
    )
  }
}

Input.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  maxLength: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired
}
