import React from 'react'
import PropTypes from 'prop-types'

export default function Header ({ children }) {
  return <div className='Header'>{children}</div>
}

Header.propTypes = {
  children: PropTypes.node.isRequired
}
