import React from 'react'
import { storiesOf } from '@storybook/react'
import Layers from './Layers'

storiesOf('Layers', module)
  .add('with one level', () => <Layers length={1}>One level</Layers>)
  .add('with two levels', () => <Layers length={2}>Two levels</Layers>)
