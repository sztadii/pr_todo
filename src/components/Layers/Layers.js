import React from 'react'
import PropTypes from 'prop-types'

function renderStateClass (length) {
  if (length > 1) return 'Layers--first Layers--second'
  else if (length) return 'Layers--first'
  else return ''
}

export default function Layers ({ length, children }) {
  return <div className={`Layers ${renderStateClass(length)}`}>{children}</div>
}

Layers.propTypes = {
  length: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired
}
