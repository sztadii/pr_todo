import React from 'react'
import renderer from 'react-test-renderer'
import Layers from './Layers'

test('Layers renders correctly with different props length values', () => {
  const component0 = renderer.create(<Layers length={0}>Something</Layers>)

  const component1 = renderer.create(<Layers length={1}>Something</Layers>)

  const component2 = renderer.create(<Layers length={2}>Something</Layers>)

  const component3 = renderer.create(<Layers length={2}>Something</Layers>)

  expect(component0.toJSON()).toMatchSnapshot()
  expect(component1.toJSON()).toMatchSnapshot()
  expect(component2.toJSON()).toMatchSnapshot()
  expect(component3.toJSON()).toMatchSnapshot()
})
