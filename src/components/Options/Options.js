import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { ESC, TAB } from 'src/config/keyCodes'
import Settings from '../Settings/Settings'
import Header from '../Header/Header'
import translations from './translations'
import connector from './OptionsConnector'

class Options extends PureComponent {
  componentDidMount () {
    document.addEventListener('keydown', this.handleKeyDown)
  }

  componentWillUnmount () {
    document.removeEventListener('keydown', this.handleKeyDown)
  }

  handleKeyDown = e => {
    if ([ESC, TAB].includes(e.keyCode)) {
      e.preventDefault()
      this.props.OptionsToggleAction()
    }
  }

  onHide = () => {
    this.props.OptionsToggleAction()
  }

  optionsActiveClass = () => (this.props.isActive ? 'Options--active' : '')

  render () {
    return (
      <div
        onClick={this.onHide}
        className={`Options ${this.optionsActiveClass()}`}
      >
        <div className='Options__background' />
        <div className='Options__content'>
          <div className='container'>
            <Header>{translations.settings[this.props.lang]}</Header>

            <div onClick={e => e.stopPropagation()}>
              <Settings />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Options.propTypes = {
  isActive: PropTypes.bool.isRequired,
  lang: PropTypes.string.isRequired,
  OptionsToggleAction: PropTypes.func.isRequired
}

export default connector(Options)
