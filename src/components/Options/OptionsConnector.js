import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { optionsIsActive, settingsLang } from 'src/store/main/selectors'
import { OptionsToggleAction } from 'src/store/actions/OptionsAction'

function mapStateToProps (state) {
  return {
    isActive: optionsIsActive(state),
    lang: settingsLang(state)
  }
}

const mapDispatchToProps = {
  OptionsToggleAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
