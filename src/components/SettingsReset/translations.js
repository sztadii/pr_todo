export default {
  areYouSure: {
    en: 'Are you sure to reset settings?',
    pl: 'Jesteś pewny by zresetować ustawienia?',
    de: 'Sind Sie sicher, dass Sie Ihre Einstellungen zurücksetzen möchten?',
    es: '¿Seguro que quieres restablecer tu configuración?'
  },
  resetSettings: {
    en: 'Reset settings',
    pl: 'Zresetuj ustawienia',
    de: 'Einstellungen zurücksetzen',
    es: 'Restablecer configuraciones'
  },
  defaultValues: {
    en: 'Reset all settings to default values',
    pl: 'Zresetuj ustawienia do wartości domyślnych',
    de: 'Einstellungen auf Standardwerte zurücksetzen',
    es: 'Restablecer la configuración a los valores predeterminados'
  }
}
