import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactTooltip from 'react-tooltip'
import { DEFAULT_COLORS, DEFAULT_LANGUAGES } from 'src/config/defaults'
import setCSS4Variable from 'src/utils/setCSS4Variable'
import Button from '../Button/Button'
import translations from './translations'
import connector from './SettingsResetConnector'

class SettingsReset extends PureComponent {
  handleReset = () => {
    const answer = confirm(translations.areYouSure[this.props.lang])

    if (answer) {
      DEFAULT_COLORS.map(color => {
        setCSS4Variable(color.name, color.value)
      })
      this.props.SettingsUpdateAction({ lang: DEFAULT_LANGUAGES[0].value })
    }
  }

  render () {
    const { lang } = this.props

    return (
      <div
        onClick={this.handleReset}
        className='SettingsReset'
        data-tip
        data-for='SettingsReset__tip'
      >
        <Button>{translations.resetSettings[lang]}</Button>

        <ReactTooltip id='SettingsReset__tip'>
          {translations.defaultValues[lang]}
        </ReactTooltip>
      </div>
    )
  }
}

SettingsReset.propTypes = {
  lang: PropTypes.string.isRequired,
  SettingsUpdateAction: PropTypes.func.isRequired
}

export default connector(SettingsReset)
