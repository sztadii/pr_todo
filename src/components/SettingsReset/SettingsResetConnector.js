import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { settingsLang } from 'src/store/main/selectors'
import { SettingsUpdateAction } from 'src/store/actions/SettingsAction'

function mapStateToProps (state) {
  return {
    lang: settingsLang(state)
  }
}

const mapDispatchToProps = {
  SettingsUpdateAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
