import React from 'react'
import PropTypes from 'prop-types'
import MainLink from '../MainLink/MainLink'
import translations from './translations'
import connector from './BackConnector'

function Back ({ lang }) {
  return (
    <div className='Back'>
      <MainLink name={translations.backToHome[lang]} path='/' />
    </div>
  )
}

Back.propTypes = {
  lang: PropTypes.string.isRequired
}

export default connector(Back)
