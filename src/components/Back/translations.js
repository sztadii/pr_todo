export default {
  backToHome: {
    en: 'Back to home',
    pl: 'Powrót do głównej',
    de: 'Zurück zur Hauptseite',
    es: 'Volver al principal'
  }
}
