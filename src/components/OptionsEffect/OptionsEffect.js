import React from 'react'
import PropTypes from 'prop-types'
import connector from './OptionsEffectConnector'

function OptionsEffect ({ isActive, children }) {
  const blurStateClass = isActive ? 'OptionsEffect--active' : ''

  return <div className={`OptionsEffect ${blurStateClass}`}>{children}</div>
}

OptionsEffect.propTypes = {
  isActive: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
}

export default connector(OptionsEffect)
