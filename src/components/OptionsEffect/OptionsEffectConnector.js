import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { optionsIsActive } from 'src/store/main/selectors'

function mapStateToProps (state) {
  return {
    isActive: optionsIsActive(state)
  }
}

export default compose(connect(mapStateToProps))
