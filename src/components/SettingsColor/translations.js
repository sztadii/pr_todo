export default {
  clickToReset: {
    en: 'Click to reset',
    pl: 'Kliknij by zresetować',
    de: 'Klicken zum Zurücksetzen',
    es: 'Haga clic para restablecer'
  },
  option: {
    en: 'Option',
    pl: 'Opcja',
    de: 'Option',
    es: 'Opción'
  }
}
