import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import CirclePicker from 'react-color/lib/components/circle/Circle'
import ReactTooltip from 'react-tooltip'
import { DEFAULT_COLORS, COLORS } from 'src/config/defaults'
import setCSS4Variable from 'src/utils/setCSS4Variable'
import translations from './translations'
import connector from './SettingsColorConnector'

class SettingsColor extends PureComponent {
  handleChangeColor = ({ hex }) => {
    setCSS4Variable(this.props.name, hex)
  }

  handleReset = () => {
    const { name } = this.props
    const defaultColor = DEFAULT_COLORS.find(color => color.name === name)
    setCSS4Variable(name, defaultColor.value)
  }

  render () {
    const { name, value, lang } = this.props

    return (
      <div className='SettingsColor'>
        <div
          onClick={this.handleReset}
          data-tip
          data-for='Settings__tip'
          className='SettingsColor__name'
        >
          {translations.option[lang]}: {name}
          <ReactTooltip id='Settings__tip'>
            {translations.clickToReset[lang]}
          </ReactTooltip>
        </div>

        <div className='SettingsColor__box shadow'>
          <CirclePicker
            color={value}
            onChangeComplete={this.handleChangeColor}
            width='100%'
            circleSize={15}
            colors={COLORS}
          />
        </div>
      </div>
    )
  }
}

SettingsColor.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  lang: PropTypes.string.isRequired
}

export default connector(SettingsColor)
