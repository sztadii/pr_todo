import React from 'react'
import { storiesOf } from '@storybook/react'
import Message from './Message'

storiesOf('Message', module)
  .add('type loading', () => <Message type={'loading'}>Loading</Message>)
  .add('type error', () => <Message type={'errors'}>Error</Message>)
