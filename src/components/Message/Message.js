import React from 'react'
import PropTypes from 'prop-types'

export default function Message ({ type, children }) {
  return <div className={`Message Message__${type} shadow`}>{children}</div>
}

Message.propTypes = {
  type: PropTypes.oneOf(['loading', 'errors']).isRequired,
  children: PropTypes.node.isRequired
}
