import React from 'react'
import PropTypes from 'prop-types'

export default function Title ({ children }) {
  return <div className='Title'>{children}</div>
}

Title.propTypes = {
  children: PropTypes.node.isRequired
}
