export default {
  whatNeedDone: {
    en: 'What needs to be done?',
    pl: 'Co powinno być zrobione?',
    de: 'Was sollte getan werden?',
    es: '¿Qué se debe hacer?'
  }
}
