import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { TEXT_MAX } from 'src/config/defaults'
import Input from '../Input/Input'
import translations from './translations'
import connector from './TodoAddConnector'

class TodoAdd extends PureComponent {
  componentDidMount () {
    this.props.TodoTextGetAction()
  }

  handleChange = text => {
    this.props.TodoTextUpdateAction(text)
  }

  addValue = e => {
    e.preventDefault()
    this.props.TodoAddAction({ name: this.props.text })
  }

  render () {
    const { text, lang } = this.props

    return (
      <form onSubmit={this.addValue} data-testid='form'>
        <Input
          value={text}
          onChange={this.handleChange}
          maxLength={TEXT_MAX}
          placeholder={translations.whatNeedDone[lang]}
        />
      </form>
    )
  }
}

TodoAdd.propTypes = {
  text: PropTypes.string.isRequired,
  lang: PropTypes.string.isRequired,
  TodoAddAction: PropTypes.func.isRequired,
  TodoTextUpdateAction: PropTypes.func.isRequired,
  TodoTextGetAction: PropTypes.func.isRequired
}

export default connector(TodoAdd)
