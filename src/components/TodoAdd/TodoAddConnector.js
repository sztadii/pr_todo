import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { todoText, settingsLang } from 'src/store/main/selectors'
import {
  TodoAddAction,
  TodoTextUpdateAction,
  TodoTextGetAction
} from 'src/store/actions/TodoAction'

function mapStateToProps (state) {
  return {
    text: todoText(state),
    lang: settingsLang(state)
  }
}

const mapDispatchToProps = {
  TodoAddAction,
  TodoTextUpdateAction,
  TodoTextGetAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
