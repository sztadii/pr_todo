import React from 'react'
import renderer from 'react-test-renderer'
import mockdate from 'mockdate'
import { Download } from './Download'

test('Download renders correctly on not empty list', () => {
  mockdate.set('10/10/2017')

  const list = [{ id: 1, name: 'Some name' }, { id: 2, name: 'Other name' }]

  const component = renderer.create(
    <Download lang='en' list={list} fileName='history' />
  )

  expect(component.toJSON()).toMatchSnapshot()
})

test('Download renders correctly on empty list', () => {
  const list = []

  const component = renderer.create(
    <Download lang='en' list={list} fileName='history' />
  )

  expect(component.toJSON()).toMatchSnapshot()
})
