import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import arrayToCsv from 'src/utils/arrayToCsv'
import Button from '../Button/Button'
import translations from './translations'
import connector from './DownloadConnector'

export class Download extends PureComponent {
  getHrefJson = () => {
    const file = encodeURIComponent(JSON.stringify(this.props.list))
    return `data: text/json;charset=utf-8,${file}`
  }

  getHrefCsv = () => {
    const file = arrayToCsv(this.props.list)
    return `data: text/csv;charset=utf-8,${file}`
  }

  getFileName = ext => {
    const { fileName } = this.props
    const prefix = moment().format('YYYY-MM-DD')
    return `${fileName}_${prefix}.${ext}`
  }

  render () {
    if (!this.props.list.length) return null

    return (
      <Button className='Download'>
        {translations.download[this.props.lang]}

        <div className='Download__hover'>
          <a
            className='Download__hoverItem'
            href={this.getHrefJson()}
            download={this.getFileName('json')}
          >
            JSON
          </a>
          <a
            className='Download__hoverItem'
            href={this.getHrefCsv()}
            download={this.getFileName('csv')}
          >
            CSV
          </a>
        </div>
      </Button>
    )
  }
}

Download.propTypes = {
  lang: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  fileName: PropTypes.string.isRequired
}

export default connector(Download)
