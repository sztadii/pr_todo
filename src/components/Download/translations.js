export default {
  download: {
    en: 'Download',
    pl: 'Pobierz',
    de: 'Herunterladen',
    es: 'Descargar'
  }
}
