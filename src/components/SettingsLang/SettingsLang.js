import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactTooltip from 'react-tooltip'
import { DEFAULT_LANGUAGES } from 'src/config/defaults'
import translations from './translations'
import connector from './SettingsLangConnector'

class SettingsLang extends PureComponent {
  handleChange = e => {
    this.props.SettingsUpdateAction({ lang: e.target.value })
  }

  render () {
    const { lang } = this.props

    return (
      <div className='SettingsLang'>
        <div
          className='SettingsLang__name'
          data-tip
          data-for='SettingLang__tip'
        >
          {translations.setLang[lang]}
        </div>

        <div className='SettingsLang__selectBox shadow'>
          <select
            value={lang}
            onChange={e => this.handleChange(e)}
            className='SettingsLang__select'
          >
            {DEFAULT_LANGUAGES.map(item => (
              <option value={item.value} key={item.value}>
                {translations[item.name][lang]} - {item.value}
              </option>
            ))}
          </select>
        </div>

        <ReactTooltip id='SettingLang__tip'>
          {translations.setLangTip[lang]}
        </ReactTooltip>
      </div>
    )
  }
}

SettingsLang.propTypes = {
  lang: PropTypes.string.isRequired,
  SettingsUpdateAction: PropTypes.func.isRequired
}

export default connector(SettingsLang)
