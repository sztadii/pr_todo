export default {
  setLang: {
    en: 'Choose lang',
    pl: 'Wybierz język',
    de: 'Wählen Sie die Sprache',
    es: 'Seleccionar idioma'
  },
  setLangTip: {
    en: 'Set lang of our application',
    pl: 'Ustaw język dla całej aplikacji',
    de: 'Sprache für die gesamte Anwendung festlegen',
    es: 'Establecer el idioma para toda la aplicación'
  },
  polish: {
    en: 'polish',
    pl: 'polski',
    de: 'polnisch',
    es: 'polaco'
  },
  english: {
    en: 'english',
    pl: 'angielski',
    de: 'englisch',
    es: 'Inglés'
  },
  german: {
    en: 'german',
    pl: 'niemiecki',
    de: 'deutsch',
    es: 'alemán'
  },
  spanish: {
    en: 'spanish',
    pl: 'hiszpański',
    de: 'spanisch',
    es: 'español'
  }
}
