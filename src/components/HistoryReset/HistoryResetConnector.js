import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { settingsLang } from 'src/store/main/selectors'
import { HistoryResetAction } from 'src/store/actions/HistoryAction'

function mapStateToProps (state) {
  return {
    lang: settingsLang(state)
  }
}

const mapDispatchToProps = {
  HistoryResetAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
