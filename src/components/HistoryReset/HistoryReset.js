import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Button from '../Button/Button'
import translations from './translations'
import connector from './HistoryResetConnector'

class HistoryReset extends PureComponent {
  handleRemove = () => {
    confirm(translations.areYouSure[this.props.lang]) &&
      this.props.HistoryResetAction()
  }

  render () {
    return (
      <div onClick={this.handleRemove} className='HistoryReset'>
        <Button>{translations.reset[this.props.lang]}</Button>
      </div>
    )
  }
}

HistoryReset.propTypes = {
  lang: PropTypes.string.isRequired,
  HistoryResetAction: PropTypes.func.isRequired
}

export default connector(HistoryReset)
