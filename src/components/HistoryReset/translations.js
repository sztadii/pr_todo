export default {
  areYouSure: {
    en: 'Are you sure to remove all history?',
    pl: 'Jesteś pewny by usunąć całą historię?',
    de: 'Sind Sie sicher, dass Sie die ganze Geschichte löschen möchten?',
    es: '¿Seguro que quieres borrar toda la historia?'
  },
  reset: {
    en: 'Reset',
    pl: 'Reset',
    de: 'Zurücksetzen',
    es: 'Reiniciar'
  }
}
