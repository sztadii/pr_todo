import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { cleanup, render, fireEvent } from 'react-testing-library'
import MainLink from './MainLink'

afterEach(cleanup)

const previewProps = {
  path: 'some-path',
  name: 'Some name',
  isPreview: true
}

const nonPreviewProps = {
  path: 'some-path',
  name: 'Some name',
  isPreview: false
}

test('MainLink contains anchor with correct href', () => {
  const { getByTestId } = render(
    <BrowserRouter>
      <MainLink {...nonPreviewProps} />
    </BrowserRouter>
  )

  const link = getByTestId('main-link')

  expect(link.href).toBe('http://localhost/some-path')
})

test('MainLink do not display preview if isPreview value is false', () => {
  const { getByTestId, queryByTestId } = render(
    <BrowserRouter>
      <MainLink {...nonPreviewProps} />
    </BrowserRouter>
  )

  const link = getByTestId('main-link')

  fireEvent(
    link,
    new MouseEvent('mouseenter', {
      bubbles: true
    })
  )

  fireEvent(
    link,
    new KeyboardEvent('keydown', {
      bubbles: true,
      keyCode: 16
    })
  )

  const preview = queryByTestId('main-link-preview-iframe')

  expect(preview).toBe(null)
})

test('MainLink do not display preview if all required events had been trigger', () => {
  const { getByTestId, queryByTestId } = render(
    <BrowserRouter>
      <MainLink {...previewProps} />
    </BrowserRouter>
  )

  const link = getByTestId('main-link')

  fireEvent(
    link,
    new MouseEvent('mouseenter', {
      bubbles: true
    })
  )

  const preview = queryByTestId('main-link-preview-iframe')

  expect(preview).toBe(null)
})

test('MainLink display preview only all required events had been trigger', () => {
  const { getByTestId } = render(
    <BrowserRouter>
      <MainLink {...previewProps} />
    </BrowserRouter>
  )

  const link = getByTestId('main-link')

  fireEvent(
    link,
    new MouseEvent('mouseenter', {
      bubbles: true
    })
  )

  fireEvent(
    link,
    new KeyboardEvent('keydown', {
      bubbles: true,
      keyCode: 16
    })
  )

  const preview = getByTestId('main-link-preview-iframe')

  expect(preview.src).toBe('http://localhost/some-path')
})
