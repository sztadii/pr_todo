import React from 'react'
import { storiesOf } from '@storybook/react'
import { MemoryRouter } from 'react-router-dom'
import MainLink from './MainLink'

const stories = storiesOf('MainLink', module)

stories.addDecorator(story => <MemoryRouter>{story()}</MemoryRouter>)

stories.add('normal usage', () => {
  return <MainLink name={'Some name'} isPreview path='/' prevY={'down'} />
})
