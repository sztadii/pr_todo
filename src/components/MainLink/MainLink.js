import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import Button from '../Button/Button'
import { SHIFT } from '../../config/keyCodes'

export default class MainLink extends PureComponent {
  state = {
    isMouse: false,
    isButton: false
  }

  componentDidMount () {
    document.addEventListener('keydown', this.handleKeyDown)
    document.addEventListener('keyup', this.handleKeyUp)
  }

  componentWillUnmount () {
    document.removeEventListener('keydown', this.handleKeyDown)
    document.removeEventListener('keyup', this.handleKeyUp)
  }

  handleKeyDown = e => {
    if ([SHIFT].includes(e.keyCode)) {
      e.preventDefault()
      this.setState({ isButton: true })
    }
  }

  handleKeyUp = e => {
    if ([SHIFT].includes(e.keyCode)) {
      e.preventDefault()
      this.setState({ isButton: false })
    }
  }

  handleMouse = () => {
    this.setState({ isMouse: !this.state.isMouse })
  }

  renderPreview = (path, prevX, prevY) => (
    <div
      className={`MainLink__preview MainLink__preview--${prevX} MainLink__preview--${prevY} shadow`}
    >
      <iframe
        src={path}
        className='MainLink__iframe'
        data-testid='main-link-preview-iframe'
      />
      <div className='MainLink__title'>{path}</div>
    </div>
  )

  renderGroupClass = isGroup => (isGroup ? 'MainLink__group' : '')

  render () {
    const { path, name, isGroup, isPreview, prevX, prevY } = this.props
    const { isMouse, isButton } = this.state

    return (
      <Link
        onMouseEnter={this.handleMouse}
        onMouseLeave={this.handleMouse}
        to={path}
        className={`MainLink ${this.renderGroupClass(isGroup)}`}
        data-testid='main-link'
      >
        <Button>{name}</Button>

        {isPreview && isMouse && isButton
          ? this.renderPreview(path, prevX, prevY)
          : null}
      </Link>
    )
  }
}

MainLink.propTypes = {
  path: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  isGroup: PropTypes.bool,
  isPreview: PropTypes.bool,
  prevX: PropTypes.oneOf(['left', 'right']),
  prevY: PropTypes.oneOf(['up', 'down'])
}

MainLink.defaultProps = {
  isGroup: false,
  isPreview: false,
  prevX: 'left',
  prevY: 'up'
}
