import React from 'react'
import PropTypes from 'prop-types'

export default function Button ({ children, type, className }) {
  return (
    <div className={`Button Button--${type} no-selection shadow ${className}`}>
      {children}
    </div>
  )
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.oneOf(['normal', 'circle']),
  className: PropTypes.string
}

Button.defaultProps = {
  className: '',
  type: 'normal'
}
