import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from './Button'

storiesOf('Button', module)
  .add('type normal', () => <Button type={'normal'}>Button</Button>)
  .add('type circle', () => <Button type={'circle'}>X</Button>)
