import React, { PureComponent } from 'react'
import ReactCssTransitionGroup from 'react-addons-css-transition-group'
import { DEFAULT_COLORS } from 'src/config/defaults'
import SettingsColor from '../SettingsColor/SettingsColor'
import SettingsLang from '../SettingsLang/SettingsLang'
import SettingsReset from '../SettingsReset/SettingsReset'

export default class Settings extends PureComponent {
  state = {
    defaultsColors: DEFAULT_COLORS
  }

  renderColors = colors =>
    colors.map(color => <SettingsColor {...color} key={color.name} />)

  render () {
    return (
      <ReactCssTransitionGroup
        transitionName='moveDownAndFade'
        transitionEnterTimeout={300}
        transitionLeaveTimeout={300}
        transitionAppear
        transitionAppearTimeout={300}
      >
        <div className='Settings'>
          <SettingsLang />

          {this.renderColors(this.state.defaultsColors)}

          <SettingsReset />
        </div>
      </ReactCssTransitionGroup>
    )
  }
}
