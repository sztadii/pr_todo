import React from 'react'
import PropTypes from 'prop-types'

export default function TodoCounter ({ error, count, isHide }) {
  const renderErrorClass = error ? 'TodoCounter--error' : ''
  const renderHideClass = isHide ? 'TodoCounter--hide' : ''

  return (
    <div
      className={`TodoCounter no-selection shadow flex-center ${renderErrorClass} ${renderHideClass}`}
      data-testid='counter'
    >
      {count}
    </div>
  )
}

TodoCounter.propTypes = {
  error: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  isHide: PropTypes.bool.isRequired
}
