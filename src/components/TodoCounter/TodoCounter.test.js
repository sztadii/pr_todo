import React from 'react'
import renderer from 'react-test-renderer'
import TodoCounter from './TodoCounter'

test('TodoCounter renders correctly', () => {
  const component = renderer.create(
    <TodoCounter count={1} error='Some error' isHide={false} />
  )

  expect(component.toJSON()).toMatchSnapshot()
})

test('TodoCounter renders when empty error', () => {
  const component = renderer.create(
    <TodoCounter count={1} error='' isHide={false} />
  )

  expect(component.toJSON()).toMatchSnapshot()
})

test('TodoCounter renders when should hide', () => {
  const component = renderer.create(<TodoCounter count={1} error='' isHide />)

  expect(component.toJSON()).toMatchSnapshot()
})
