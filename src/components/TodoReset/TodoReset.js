import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Button from '../Button/Button'
import translations from './translations'
import connector from './TodoResetConnector'

class TodoReset extends PureComponent {
  resetStorage = () => {
    confirm(translations.areYouSure[this.props.lang]) &&
      this.props.TodoResetAction()
  }

  render () {
    return (
      <div onClick={this.resetStorage} className='TodoReset'>
        <Button>{translations.reset[this.props.lang]}</Button>
      </div>
    )
  }
}

TodoReset.propTypes = {
  lang: PropTypes.string.isRequired,
  TodoResetAction: PropTypes.func.isRequired
}

export default connector(TodoReset)
