export default {
  areYouSure: {
    en: 'Aure you sure to reset all?',
    pl: 'Jesteś pewny by wszystko zerestować?',
    de: 'Sind Sie sicher, dass Sie es überprüfen möchten?',
    es: '¿Estás seguro de que quieres verificarlo?'
  },
  reset: {
    en: 'Reset',
    pl: 'Reset',
    de: 'Rücksetzen',
    es: 'Reajustar'
  }
}
