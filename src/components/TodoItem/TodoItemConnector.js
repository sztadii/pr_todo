import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { settingsLang } from 'src/store/main/selectors'
import { TodoRemoveAction } from 'src/store/actions/TodoAction'

function mapStateToProps (state) {
  return {
    lang: settingsLang(state)
  }
}

const mapDispatchToProps = {
  TodoRemoveAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
