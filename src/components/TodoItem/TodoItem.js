import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactTooltip from 'react-tooltip'
import showLastSignsInString from 'src/utils/showLastSignsInString'
import copyValueToClipboard from 'src/utils/copyValueToClipboard'
import Button from '../Button/Button'
import translations from './translations'
import connector from './TodoItemConnector'

class TodoItem extends PureComponent {
  static handleCopy (value) {
    copyValueToClipboard(value)
  }

  handleRemove = () => {
    confirm(translations.areYouSure[this.props.lang]) &&
      this.props.TodoRemoveAction(this.props.id)
  }

  render () {
    const { id, name, lang } = this.props

    return (
      <div className='TodoItem' data-testid='todo-item'>
        <div
          onClick={() => TodoItem.handleCopy(id)}
          className='TodoItem__id'
          data-tip
          data-for={`TodoItem__id__${id}`}
        >
          Id: {showLastSignsInString(id.toString(), 4)}
          <ReactTooltip id={`TodoItem__id__${id}`}>
            {translations.clickToCopy[lang]} id
          </ReactTooltip>
        </div>

        <div
          onClick={() => TodoItem.handleCopy(name)}
          className='TodoItem__name'
          data-tip
          data-for={`TodoItem__name__${id}`}
        >
          Name: {name}
          <ReactTooltip id={`TodoItem__name__${id}`}>
            {translations.clickToCopy[lang]} name
          </ReactTooltip>
        </div>

        <div onClick={this.handleRemove} className='TodoItem__close'>
          <Button type='circle'>X</Button>
        </div>
      </div>
    )
  }
}

TodoItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  TodoRemoveAction: PropTypes.func.isRequired,
  lang: PropTypes.string.isRequired
}

export default connector(TodoItem)
