export default {
  areYouSure: {
    en: 'Are you sure to remove this item?',
    pl: 'Jesteś pewny by usunąć ten element?',
    de: 'Möchten Sie diesen Artikel wirklich löschen?',
    es: '¿Seguro que quieres eliminar este artículo?'
  },
  clickToCopy: {
    en: 'Click to copy',
    pl: 'Kliknij by skopiować',
    de: 'Zum Kopieren anklicken',
    es: 'Haga clic para copiar'
  }
}
