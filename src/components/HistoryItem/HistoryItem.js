import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactTooltip from 'react-tooltip'
import showLastSignsInString from 'src/utils/showLastSignsInString'
import copyValueToClipboard from 'src/utils/copyValueToClipboard'
import indexToDigitFormat from 'src/utils/indexToDigitFormat'
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now'
import pl from 'date-fns/locale/pl'
import en from 'date-fns/locale/en'
import Button from '../Button/Button'
import translations from './translations'
import connector from './HistoryItemConnector'

export class HistoryItem extends PureComponent {
  handleCopy = value => {
    copyValueToClipboard(value)
  }

  handleRemove = () => {
    confirm(translations.areYouSure[this.props.lang]) &&
      this.props.HistoryRemoveAction(this.props.id)
  }

  displayTimeDistance = timestamp => {
    const locale = { pl, en }
    return distanceInWordsToNow(new Date(timestamp), {
      locale: locale[this.props.lang]
    })
  }

  render () {
    const { i, id, name, lang } = this.props

    return (
      <div className='HistoryItem'>
        <div className='HistoryItem__block HistoryItem__block--black'>
          {indexToDigitFormat(i, 2)}:
        </div>

        <div
          onClick={() => this.handleCopy(id)}
          className='HistoryItem__block'
          data-tip
          data-for={`HistoryItem__id__${id}`}
        >
          Id: {showLastSignsInString(id.toString(), 4)}
          <ReactTooltip id={`HistoryItem__id__${id}`}>
            {translations.clickToCopy[lang]} id
          </ReactTooltip>
        </div>

        <div
          className='HistoryItem__name'
          data-tip
          data-for={`HistoryItem__date__${id}`}
        >
          Name: {name}
          <ReactTooltip id={`HistoryItem__date__${id}`}>
            {translations.date[lang]}: {this.displayTimeDistance(id)}
          </ReactTooltip>
        </div>

        <div onClick={this.handleRemove} className='HistoryItem__close'>
          <Button type={'circle'}>X</Button>
        </div>
      </div>
    )
  }
}

HistoryItem.propTypes = {
  i: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  lang: PropTypes.string.isRequired,
  HistoryRemoveAction: PropTypes.func.isRequired
}

export default connector(HistoryItem)
