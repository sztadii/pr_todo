import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { settingsLang } from 'src/store/main/selectors'
import { HistoryRemoveAction } from 'src/store/actions/HistoryAction'

function mapStateToProps (state) {
  return {
    lang: settingsLang(state)
  }
}

const mapDispatchToProps = {
  HistoryRemoveAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
