import React from 'react'
import moment from 'moment'
import mockdate from 'mockdate'
import renderer from 'react-test-renderer'
import { HistoryItem } from './HistoryItem'

test('HistoryItem renders correctly', () => {
  mockdate.set(moment('7/11/2017', 'DD/MM/YYYY'))

  const component = renderer.create(
    <HistoryItem
      i={1}
      name={'Some name'}
      id={1234}
      lang={'en'}
      HistoryRemoveAction={() => {}}
    />
  )

  expect(component.toJSON()).toMatchSnapshot()
})
