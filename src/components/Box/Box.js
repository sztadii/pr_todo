import React from 'react'
import PropTypes from 'prop-types'

export default function Box ({ children }) {
  return <div className='Box shadow'>{children}</div>
}

Box.propTypes = {
  children: PropTypes.node.isRequired
}
