import React from 'react'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { render } from 'react-testing-library'
import reducer from '../store/reducers'
import saga from '../store/middleware/index'

const sagaMiddleware = createSagaMiddleware()

function getStore (initialState) {
  return createStore(reducer, initialState, applyMiddleware(sagaMiddleware))
}

export default function renderWithRedux (
  ui,
  { initialState, store = getStore(initialState) } = {}
) {
  sagaMiddleware.run(saga)
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store
  }
}
