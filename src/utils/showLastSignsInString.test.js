import showLastSignsInString from './showLastSignsInString'

test('showLastSignsInString should return shorted last 3 signs', () => {
  const string = 'Some string'
  const number = 3
  const expectString = '...ing'

  expect(showLastSignsInString(string, number)).toEqual(expectString)
})

test('showLastSignsInString should return not shorted string', () => {
  const string = 'Some string'
  const number = 0

  expect(showLastSignsInString(string)).toEqual(string)
  expect(showLastSignsInString(string, number)).toEqual(string)
})
