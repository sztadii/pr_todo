export default function copyValueToClipboard (value) {
  const tmpInput = document.createElement('input')
  tmpInput.setAttribute('value', value)
  document.body.appendChild(tmpInput)
  tmpInput.select()
  document.execCommand('copy')
  document.body.removeChild(tmpInput)
}
