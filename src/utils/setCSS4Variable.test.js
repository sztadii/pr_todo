import setCSS4Variable from './setCSS4Variable'

test('setCSS4Variable should return undefined', () => {
  const name = 'gray'
  const value = '#404040'

  expect(setCSS4Variable()).toEqual(undefined)
  expect(setCSS4Variable(name, value)).toEqual(undefined)
})
