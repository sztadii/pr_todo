import indexToDigitFormat from './indexToDigitFormat'

test('indexToDigitFormat should return formatted value', () => {
  expect(indexToDigitFormat(0, 2)).toEqual('01')
  expect(indexToDigitFormat(2, 3)).toEqual('003')
})
