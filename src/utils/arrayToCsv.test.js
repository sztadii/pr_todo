import arrayToCsv from './arrayToCsv'

test('arrayToCsv should return transformed array to csv format', () => {
  const array = [{ id: 1, name: 'Some name' }, { id: 2, name: 'Other name' }]

  expect(arrayToCsv(array)).toMatchSnapshot()
})
