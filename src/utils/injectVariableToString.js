export default function injectVariableToString (str, variable) {
  return str.replace(/XXX/gi, variable)
}
