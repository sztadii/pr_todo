export default function showLastSignsInString (string, number) {
  if (number > 0) {
    const { length } = string
    return '...' + string.slice(length - number, length)
  }

  return string
}
