import padStart from 'lodash/padStart'

export default function indexToDigitFormat (number, digits) {
  return padStart(number + 1, digits, '0')
}
