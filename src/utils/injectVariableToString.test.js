import injectVariableToString from './injectVariableToString'

test('injectVariableToString should return english text with injected variable', () => {
  const string = 'You earn XXX apples'
  const variable = 10

  expect(injectVariableToString(string, variable)).toEqual('You earn 10 apples')
})

test('injectVariableToString should return arabic text with injected variable', () => {
  const string = 'تكسب XXX التفاح'
  const variable = 10

  expect(injectVariableToString(string, variable)).toEqual('تكسب 10 التفاح')
})
