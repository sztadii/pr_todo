import matchAppLanguageWithBrowserLanguage from './matchAppLanguageWithBrowserLanguage'

const appLanguages = ['en', 'pl', 'de']

test('matchAppLanguageWithBrowser should return "pl" when browser language is "pl" or "pl-pl" and etc', () => {
  const navigator = { language: 'pl-pl' }
  const expectValue = 'pl'
  expect(matchAppLanguageWithBrowserLanguage(appLanguages, navigator)).toEqual(
    expectValue
  )
})

test('matchAppLanguageWithBrowser should return "en" when browser language can not match with appLanguages', () => {
  const navigator = { language: 'fr-fr' }
  const expectValue = 'en'
  expect(matchAppLanguageWithBrowserLanguage(appLanguages, navigator)).toEqual(
    expectValue
  )
})
