export default function setCSS4Variable (name, value) {
  document.documentElement.style.setProperty(`--${name}`, value)
}
