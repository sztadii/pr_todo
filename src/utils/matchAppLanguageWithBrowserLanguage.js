export default function matchAppLanguageWithBrowserLanguage (
  appLanguages,
  navigator
) {
  const lang = appLanguages.find(item => navigator.language.includes(item))
  return lang || appLanguages[0]
}
