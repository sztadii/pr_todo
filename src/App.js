import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import loadable from 'loadable-components'
import Header from './components/Header/Header'
import Nav from './components/Nav/Nav'
import Options from './components/Options/Options'
import OptionsEffect from './components/OptionsEffect/OptionsEffect'
import routes from './config/routes'

const Todo = loadable(() => import('./routes/Todo/Todo'))
const History = loadable(() => import('./routes/History/History'))
const Stats = loadable(() => import('./routes/Stats/Stats'))
const NoMatch = loadable(() => import('./routes/NoMatch/NoMatch'))

export default function App ({ location }) {
  return (
    <div>
      <OptionsEffect>
        <div className='container'>
          <Header>Todo</Header>

          <Route
            location={location}
            key={location.key}
            component={({ match }) => (
              <Switch>
                <Route exact path={`${match.path}`} component={Todo} />
                <Route
                  path={`${match.path}${routes.history}`}
                  component={History}
                />
                <Route
                  path={`${match.path}${routes.stats}`}
                  component={Stats}
                />
                <Route component={NoMatch} />
              </Switch>
            )}
          />

          <Nav />
        </div>
      </OptionsEffect>

      <Options />
    </div>
  )
}

App.propTypes = {
  location: PropTypes.object.isRequired
}
