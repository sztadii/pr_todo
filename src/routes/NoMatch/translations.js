export default {
  noMatch: {
    en: 'No find page',
    pl: 'Nie znaleziono strony',
    de: 'Seite nicht gefunden',
    es: 'Página no encontrada'
  }
}
