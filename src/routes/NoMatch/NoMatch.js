import React from 'react'
import PropTypes from 'prop-types'
import Title from '../../components/Title/Title'
import Back from '../../components/Back/Back'
import translations from './translations'
import connector from './NoMatchConnector'

function NoMatch ({ lang }) {
  return (
    <div className='NoMatch'>
      <Title>{translations.noMatch[lang]}</Title>
      <Back />
    </div>
  )
}

NoMatch.propTypes = {
  lang: PropTypes.string.isRequired
}

export default connector(NoMatch)
