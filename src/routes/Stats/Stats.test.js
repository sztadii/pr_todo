import moment from 'moment'
import { Stats } from './Stats'

test('chartFilter transform data correctly', () => {
  const date = moment('24/10/2017', 'DD/MM/YYYY')
    .toDate()
    .getTime()

  const array = [
    { id: date, name: 'Some name' },
    { id: date, name: 'Other name' }
  ]

  const expectObject = {
    labels: ['24/10/2017'],
    data: [2]
  }

  expect(Stats.chartFilter(array)).toEqual(expectObject)
})
