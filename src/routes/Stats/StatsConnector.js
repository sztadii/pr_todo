import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import {
  historyLoading,
  historyError,
  historyList,
  settingsLang
} from 'src/store/main/selectors'
import { HistoryGetAction } from 'src/store/actions/HistoryAction'

function mapStateToProps (state) {
  return {
    loading: historyLoading(state),
    error: historyError(state),
    list: historyList(state),
    lang: settingsLang(state)
  }
}

const mapDispatchToProps = {
  HistoryGetAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
