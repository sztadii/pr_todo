import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'
import uniq from 'lodash/uniq'
import Box from '../../components/Box/Box'
import Button from '../../components/Button/Button'
import Back from '../../components/Back/Back'
import Title from '../../components/Title/Title'
import translations from './translations'
import connector from './StatsConnector'

export class Stats extends PureComponent {
  componentDidMount () {
    this.props.HistoryGetAction()
  }

  static chartFilter (array) {
    const format = 'DD/MM/YYYY'
    const allDates = array.map(item => moment(item.id).format(format))
    const labels = uniq(allDates)
    const data = []

    labels.forEach(label => {
      const filtered = array.filter(item =>
        moment(item.id).isSame(moment(label, format), 'day')
      )
      data.push(filtered.length)
    })

    return {
      labels,
      data
    }
  }

  static generateChart (list) {
    const obj = Stats.chartFilter(list)

    const data = {
      labels: obj.labels,
      datasets: [
        {
          label: 'Dates of todos',
          data: obj.data
        }
      ]
    }

    const options = {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }

    return (
      <div className='Stats__chart'>
        <Bar data={data} options={options} />
      </div>
    )
  }

  static downloadCanvas (link, filename) {
    link.href = document.querySelector('canvas').toDataURL()
    link.download = filename
  }

  save = () => {
    Stats.downloadCanvas(
      document.querySelector('.Stats__download'),
      'canvas.png'
    )
  }

  renderDownload = () => (
    <a onClick={this.save} className='Stats__download'>
      <Button>{translations.save[this.props.lang]}</Button>
    </a>
  )

  render () {
    const { lang, list } = this.props
    const { length } = list

    return (
      <div className='Stats'>
        <Box>
          <Title>{translations.dateStats[lang]}</Title>

          {length ? this.renderDownload() : null}

          {length ? Stats.generateChart(list) : null}
        </Box>

        {!length ? <Back /> : null}
      </div>
    )
  }
}

Stats.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired
    })
  ).isRequired,
  lang: PropTypes.string.isRequired,
  HistoryGetAction: PropTypes.func.isRequired
}

export default connector(Stats)
