export default {
  dateStats: {
    en: 'Daily statistics',
    pl: 'Dzienne statystyki',
    de: 'Tägliche Statistiken',
    es: 'Estadísticas diarias'
  },
  save: {
    en: 'Save',
    pl: 'Zapisz',
    de: 'Sparen',
    es: 'Salvar'
  }
}
