import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactCssTransitionGroup from 'react-addons-css-transition-group'
import TodoItem from '../../components/TodoItem/TodoItem'
import TodoAdd from '../../components/TodoAdd/TodoAdd'
import TodoReset from '../../components/TodoReset/TodoReset'
import TodoCounter from '../../components/TodoCounter/TodoCounter'
import Download from '../../components/Download/Download'
import Message from '../../components/Message/Message'
import Box from '../../components/Box/Box'
import Layers from '../../components/Layers/Layers'
import connector from './TodoConnector'

class Todo extends PureComponent {
  state = {
    listActive: false
  }

  componentDidMount () {
    this.props.TodoGetAction()
  }

  handleCounterChange = () => {
    this.setState({
      listActive: !this.state.listActive
    })
  }

  renderList = list => (
    <ReactCssTransitionGroup
      transitionName='moveDownAndFade'
      transitionEnterTimeout={300}
      transitionLeaveTimeout={300}
      transitionAppear
      transitionAppearTimeout={300}
    >
      {list.map(item => <TodoItem {...item} key={item.id} />)}
    </ReactCssTransitionGroup>
  )

  render () {
    const { isLoading, error, list } = this.props
    const { listActive } = this.state
    const { length } = list

    return (
      <Layers length={length}>
        <Box>
          <TodoAdd />

          {length ? <TodoReset /> : null}

          <div
            onMouseEnter={this.handleCounterChange}
            onMouseLeave={this.handleCounterChange}
            className='Todo__list'
          >
            {length ? this.renderList(list) : null}
          </div>

          {length ? (
            <TodoCounter isHide={listActive} error={error} count={length} />
          ) : null}
        </Box>

        <Download list={list} fileName='todo' />

        {isLoading ? <Message type='loading'>Loading</Message> : null}

        {error ? <Message type='errors'>{error}</Message> : null}
      </Layers>
    )
  }
}

Todo.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired
    })
  ).isRequired,
  TodoGetAction: PropTypes.func.isRequired,
  SettingsGetAction: PropTypes.func.isRequired
}

export default connector(Todo)
