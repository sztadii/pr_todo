import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { todoError, todoLoading, todoList } from 'src/store/main/selectors'
import { TodoGetAction } from 'src/store/actions/TodoAction'
import { SettingsGetAction } from 'src/store/actions/SettingsAction'

function mapStateToProps (state) {
  return {
    isLoading: todoLoading(state),
    error: todoError(state),
    list: todoList(state)
  }
}

const mapDispatchToProps = {
  TodoGetAction,
  SettingsGetAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
