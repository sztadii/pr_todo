import React from 'react'
import { fireEvent, cleanup } from 'react-testing-library'
import renderWithStore from '../../utils/renderWithStore'
import Todo from './Todo'

afterEach(() => {
  cleanup()
  localStorage.clear()
})

test('Todo page display placeholder in defined language', () => {
  const { getByPlaceholderText } = renderWithStore(<Todo />)
  getByPlaceholderText('What needs to be done?')
})

test('Todo page display message with TodoReducer contains any errors', () => {
  const wrapper = renderWithStore(<Todo />)
  const input = wrapper.getByPlaceholderText('What needs to be done?')
  const form = wrapper.getByTestId('form')
  input.value = ''
  fireEvent.submit(form)
  wrapper.getByText('Empty text!')
})

test('Todo page display many validation errors', () => {
  const wrapper = renderWithStore(<Todo />)
  const input = wrapper.getByPlaceholderText('What needs to be done?')
  const form = wrapper.getByTestId('form')
  input.value = ''
  fireEvent.submit(form)
  wrapper.getByText('Empty text!')
})

test('Todo page add new todo with proper value', () => {
  const wrapper = renderWithStore(<Todo />)
  const input = wrapper.getByPlaceholderText('What needs to be done?')
  const form = wrapper.getByTestId('form')

  input.value = 'First todo'
  fireEvent.input(input)
  fireEvent.submit(form)

  wrapper.getByText(/First todo/)
})

test('Todo page contains proper number of todos', () => {
  const wrapper = renderWithStore(<Todo />)
  const input = wrapper.getByPlaceholderText('What needs to be done?')
  const form = wrapper.getByTestId('form')

  input.value = 'First todo'
  fireEvent.input(input)
  fireEvent.submit(form)

  input.value = 'Second todo'
  fireEvent.input(input)
  fireEvent.submit(form)

  const todo = wrapper.getAllByTestId('todo-item')

  expect(todo).toHaveLength(2)
})

test('Todo counter display proper length of todos', () => {
  const wrapper = renderWithStore(<Todo />)
  const input = wrapper.getByPlaceholderText('What needs to be done?')
  const form = wrapper.getByTestId('form')

  input.value = 'First todo'
  fireEvent.input(input)
  fireEvent.submit(form)

  input.value = 'Second todo'
  fireEvent.input(input)
  fireEvent.submit(form)

  const counter = wrapper.getByTestId('counter')
  expect(counter.textContent).toBe('2')
})
