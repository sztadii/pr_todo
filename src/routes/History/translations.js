export default {
  search: {
    en: 'Search',
    pl: 'Szukaj',
    de: 'Suche',
    es: 'Búsqueda'
  },
  lastTodo: {
    en: 'Last todo',
    pl: 'Ostatnie loto',
    de: 'Letzte todo',
    es: 'Último todo'
  },
  olderTodo: {
    en: 'Older todo',
    pl: 'Starsze todo',
    de: 'Älteres todo',
    es: 'Todo más antiguo'
  },
  emptyList: {
    en: 'Empty list',
    pl: 'Pusta list',
    de: 'Leere Liste',
    es: 'Lista vacía'
  }
}
