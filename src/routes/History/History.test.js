import moment from 'moment'
import mockdate from 'mockdate'
import History from './History'

test('History isListOlderThanOneDay should return true if list is older than one day', () => {
  const day = moment('6/11/2017', 'DD/MM/YYYY')
    .toDate()
    .getTime()

  mockdate.set(moment('7/11/2017', 'DD/MM/YYYY'))

  const list = [{ id: day }, { id: day }]

  expect(History.isListOlderThanOneDay(list)).toEqual(true)
})

test('History isListOlderThanOneDay should return false if list is not older than one day', () => {
  const day = moment('6/11/2017', 'DD/MM/YYYY')
    .toDate()
    .getTime()

  mockdate.set(moment('6/11/2017', 'DD/MM/YYYY'))

  const list = [{ id: day }, { id: day }]

  expect(History.isListOlderThanOneDay(list)).toEqual(false)
})
