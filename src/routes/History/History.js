import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactCssTransitionGroup from 'react-addons-css-transition-group'
import moment from 'moment/moment'
import HistoryItem from '../../components/HistoryItem/HistoryItem'
import HistoryReset from '../../components/HistoryReset/HistoryReset'
import Title from '../../components/Title/Title'
import Message from '../../components/Message/Message'
import Box from '../../components/Box/Box'
import Layers from '../../components/Layers/Layers'
import Back from '../../components/Back/Back'
import Download from '../../components/Download/Download'
import translations from './translations'
import connector from './HistoryConnector'

class History extends PureComponent {
  componentDidMount () {
    this.props.HistoryGetAction()
  }

  static isListOlderThanOneDay (list) {
    const beforeElement = list.filter(item =>
      moment(item.id).isBefore(moment().format(), 'day')
    )
    return !!beforeElement.length
  }

  renderList = list => (
    <ReactCssTransitionGroup
      transitionName='moveDownAndFade'
      transitionEnterTimeout={300}
      transitionLeaveTimeout={300}
      transitionAppear
      transitionAppearTimeout={300}
    >
      {list.map((item, i) => <HistoryItem i={i} {...item} key={item.id} />)}
    </ReactCssTransitionGroup>
  )

  renderTitle = () => {
    const { list, lang } = this.props
    if (History.isListOlderThanOneDay(list)) return translations.olderTodo[lang]
    return list.length
      ? translations.lastTodo[lang]
      : translations.emptyList[lang]
  }

  render () {
    const { isLoading, error, list } = this.props
    const { length } = list

    return (
      <Layers length={length}>
        <Box>
          <Title>{this.renderTitle()}</Title>

          {length ? <HistoryReset /> : null}

          {length ? this.renderList(list) : null}
        </Box>

        <Download list={list} fileName='history' />

        {!length ? <Back /> : null}

        {isLoading ? <Message type='loading'>Loading</Message> : null}

        {error ? <Message type='errors'>{error}</Message> : null}
      </Layers>
    )
  }
}

History.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  ).isRequired,
  lang: PropTypes.string.isRequired,
  HistoryGetAction: PropTypes.func.isRequired
}

export default connector(History)
