import { put, call } from 'redux-saga/effects'
import db from '../../../db/db'
import * as actions from '../../actions/HistoryAction'

export function * historyGet () {
  yield put(actions.HistoryGetLoadingAction())

  try {
    const data = yield call(db.get, `history-${db.objects.list}`)
    const history = data || []
    yield put(actions.HistoryGetSuccessAction(history))
  } catch (e) {
    yield put(actions.HistoryErrorAction(e.message))
  }
}

export function * historyRemove (action) {
  try {
    const id = yield call(db.remove, `history-${db.objects.list}`, action.id)
    yield put(actions.HistoryRemoveSuccessAction(id))
  } catch (e) {
    yield put(actions.HistoryErrorAction(e.message))
  }
}

export function * historyReset () {
  try {
    yield call(db.resetObject, `history-${db.objects.list}`)
    yield put(actions.HistoryResetSuccessAction())
  } catch (e) {
    yield put(actions.HistoryErrorAction(e.message))
  }
}
