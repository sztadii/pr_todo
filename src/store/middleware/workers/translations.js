export default {
  limit: {
    en: 'Limit is XXX!',
    pl: 'Ograniczenie jest XXX!',
    de: 'Die Einschränkung ist XXX!',
    es: '¡La restricción es XXX!'
  },
  empty: {
    en: 'Empty text!',
    pl: 'Pusty tekst!',
    de: 'Leerer Text!',
    es: '¡Texto vacío!'
  },
  minimum: {
    en: 'Minimum is XXX!',
    pl: 'Minimum jest XXX!',
    de: 'Das Minimum ist XXX!',
    es: '¡El mínimo es XXX!'
  },
  text: {
    en: 'Text limit is XXX!',
    pl: 'Limit tekstu jest XXX!',
    de: 'Das Textlimit ist XXX!',
    es: '¡El límite de texto es XXX!'
  }
}
