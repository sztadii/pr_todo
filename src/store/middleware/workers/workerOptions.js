/* eslint-disable import/prefer-default-export  */
import { put } from 'redux-saga/effects'
import * as actions from '../../actions/OptionsAction'

export function * optionsToggle () {
  try {
    yield put(actions.OptionsToggleSuccessAction())
  } catch (e) {
    yield put(actions.OptionsToggleErrorAction(e.message))
  }
}
