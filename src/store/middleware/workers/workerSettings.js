import { put, call } from 'redux-saga/effects'
import db from '../../../db/db'
import * as actions from '../../actions/SettingsAction'
import { DEFAULT_LANGUAGES } from '../../../config/defaults'
import matchAppLanguageWithBrowserLanguage from '../../../utils/matchAppLanguageWithBrowserLanguage'

export function * settingsGet () {
  try {
    let data = yield call(db.get, db.objects.settings)

    if (!data || !data.lang) {
      const appLanguages = DEFAULT_LANGUAGES.map(item => item.value)

      data = {
        lang: matchAppLanguageWithBrowserLanguage(appLanguages, navigator)
      }
    }

    yield put(actions.SettingsGetSuccessAction(data))
  } catch (e) {
    yield put(actions.SettingsErrorAction(e.message))
  }
}

export function * settingsUpdate (action) {
  try {
    const data = yield call(db.set, db.objects.settings, action.settings)
    yield put(actions.SettingsUpdateSuccessAction(data))
  } catch (e) {
    yield put(actions.SettingsErrorAction(e.message))
  }
}
