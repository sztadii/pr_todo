import { put, call, select } from 'redux-saga/effects'
import db from '../../../db/db'
import { LIST_MAX, TEXT_MIN, TEXT_MAX } from '../../../config/defaults'
import * as actions from '../../actions/TodoAction'
import { todoList, settingsLang } from '../../main/selectors'
import injectVariableToString from '../../../utils/injectVariableToString'
import translations from './translations'

export function * todoGet () {
  yield put(actions.TodoGetLoadingAction())

  try {
    const data = yield call(db.get, db.objects.list)
    const todo = data || []
    yield put(actions.TodoGetSuccessAction(todo))
  } catch (e) {
    yield put(actions.TodoErrorAction(e.message))
  }
}

export function * todoAdd (action) {
  const state = yield select()
  const list = todoList(state)
  const lang = settingsLang(state)

  if (list.length >= LIST_MAX) {
    const messageLimit = injectVariableToString(
      translations.limit[lang],
      LIST_MAX
    )
    yield put(actions.TodoErrorAction(messageLimit))
  } else if (!action.item.name) {
    const messageEmpty = translations.empty[lang]
    yield put(actions.TodoErrorAction(messageEmpty))
  } else if (action.item.name.length < TEXT_MIN) {
    const messageMin = injectVariableToString(
      translations.minimum[lang],
      TEXT_MIN
    )
    yield put(actions.TodoErrorAction(messageMin))
  } else {
    try {
      const item = yield call(db.push, db.objects.list, action.item)
      yield put(actions.TodoAddSuccessAction(item))
      yield put(actions.TodoTextUpdateAction())
    } catch (e) {
      yield put(actions.TodoErrorAction(e.message))
    }
  }
}

export function * todoRemove (action) {
  try {
    const id = yield call(db.remove, db.objects.list, action.id)
    yield put(actions.TodoRemoveSuccessAction(id))
  } catch (e) {
    yield put(actions.TodoErrorAction(e.message))
  }
}

export function * todoReset () {
  try {
    yield call(db.reset)
    yield put(actions.TodoResetSuccessAction())
  } catch (e) {
    yield put(actions.TodoErrorAction(e.message))
  }
}

export function * textGet () {
  try {
    const data = yield call(db.get, db.objects.text)
    const text = data || ''
    yield put(actions.TodoTextGetSuccessAction(text))
  } catch (e) {
    yield put(actions.TodoErrorAction(e.message))
  }
}

export function * textUpdate (action) {
  const state = yield select()
  const lang = settingsLang(state)
  const text = action.text

  if (text.length >= TEXT_MAX) {
    const messageLimit = injectVariableToString(
      translations.text[lang],
      TEXT_MAX
    )
    yield put(actions.TodoErrorAction(messageLimit))
  } else {
    try {
      const data = yield call(db.set, db.objects.text, text)
      yield put(actions.TodoTextUpdateSuccessAction(data))
    } catch (e) {
      yield put(actions.TodoErrorAction(e.message))
    }
  }
}
