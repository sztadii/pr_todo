import { fork } from 'redux-saga/effects'
import watchTodo from './watchers/watchTodo'
import watchHistory from './watchers/watchHistory'
import watchSettings from './watchers/watchSettings'
import watchOptions from './watchers/watchOptions'

export default function * () {
  yield [
    fork(watchTodo),
    fork(watchHistory),
    fork(watchSettings),
    fork(watchOptions)
  ]
}
