import { takeLatest } from 'redux-saga/effects'
import * as actions from '../../actions/TodoAction'
import * as workers from '../workers/workerTodo'

export default function * watchTodo () {
  yield takeLatest(actions.TODO_GET, workers.todoGet)
  yield takeLatest(actions.TODO_ADD, workers.todoAdd)
  yield takeLatest(actions.TODO_REMOVE, workers.todoRemove)
  yield takeLatest(actions.TODO_RESET, workers.todoReset)
  yield takeLatest(actions.TODO_TEXT_GET, workers.textGet)
  yield takeLatest(actions.TODO_TEXT_UPDATE, workers.textUpdate)
}
