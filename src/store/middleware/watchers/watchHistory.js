import { takeLatest } from 'redux-saga/effects'
import * as actions from '../../actions/HistoryAction'
import * as workers from '../workers/workerHistory'

export default function * watchHistory () {
  yield takeLatest(actions.HISTORY_GET, workers.historyGet)
  yield takeLatest(actions.HISTORY_REMOVE, workers.historyRemove)
  yield takeLatest(actions.HISTORY_RESET, workers.historyReset)
}
