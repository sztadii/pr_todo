import { takeLatest } from 'redux-saga/effects'
import * as actions from '../../actions/SettingsAction'
import * as workers from '../workers/workerSettings'

export default function * watchSettings () {
  yield takeLatest(actions.SETTINGS_GET, workers.settingsGet)
  yield takeLatest(actions.SETTINGS_UPDATE, workers.settingsUpdate)
}
