import { takeLatest } from 'redux-saga/effects'
import * as actions from '../../actions/OptionsAction'
import * as workers from '../workers/workerOptions'

export default function * watchOptions () {
  yield takeLatest(actions.OPTIONS_TOGGLE, workers.optionsToggle)
}
