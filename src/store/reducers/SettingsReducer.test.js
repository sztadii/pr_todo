import * as actions from '../actions/SettingsAction'
import SettingsReducer from './SettingsReducer'

test('SettingsReducer return default state on init', () => {
  const expectState = {
    lang: 'en',
    error: ''
  }
  expect(SettingsReducer(undefined, {})).toEqual(expectState)
})

test('SettingsReducer return error state after trigger LangErrorAction', () => {
  const message = 'Error message'
  const expectState = {
    lang: 'en',
    error: message
  }
  expect(
    SettingsReducer(undefined, actions.SettingsErrorAction(message))
  ).toEqual(expectState)
})

test('SettingsReducer return GET state after trigger SettingsGetSuccessAction', () => {
  const settings = {
    lang: 'en'
  }
  const expectState = {
    lang: 'en',
    error: ''
  }
  expect(
    SettingsReducer(undefined, actions.SettingsGetSuccessAction(settings))
  ).toEqual(expectState)
})

test('SettingsReducer return updated state after trigger SettingsUpdateSuccessAction', () => {
  const newSettings = {
    lang: 'pl'
  }
  const initState = {
    lang: 'en',
    error: ''
  }
  const expectState = {
    ...newSettings,
    error: ''
  }
  expect(
    SettingsReducer(initState, actions.SettingsUpdateSuccessAction(newSettings))
  ).toEqual(expectState)
})
