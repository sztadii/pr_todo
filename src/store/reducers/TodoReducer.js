/* eslint-disable max-lines-per-function */
import * as todo from '../actions/TodoAction'
import { initState } from '../main/initState'

const init = initState.TodoReducer

export default function TodoReducer (state = init, action) {
  switch (action.type) {
    case todo.TODO_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error
      }
    }

    case todo.TODO_GET_LOADING: {
      return {
        ...state,
        loading: true,
        error: ''
      }
    }

    case todo.TODO_GET_SUCCESS: {
      return {
        ...state,
        list: action.list,
        loading: false
      }
    }

    case todo.TODO_ADD_SUCCESS: {
      return {
        ...state,
        list: [action.item, ...state.list],
        loading: false,
        error: ''
      }
    }

    case todo.TODO_REMOVE_SUCCESS: {
      return {
        ...state,
        list: state.list.filter(item => item.id !== action.id),
        loading: false,
        error: ''
      }
    }

    case todo.TODO_RESET_SUCCESS: {
      return init
    }

    case todo.TODO_TEXT_GET_SUCCESS: {
      return {
        ...state,
        text: action.text
      }
    }

    case todo.TODO_TEXT_UPDATE_SUCCESS: {
      return {
        ...state,
        text: action.text,
        error: ''
      }
    }
  }

  return state
}
