import { combineReducers } from 'redux'
import TodoReducer from './TodoReducer'
import SettingsReducer from './SettingsReducer'
import HistoryReducer from './HistoryReducer'
import OptionsReducer from './OptionsReducer'

export default combineReducers({
  TodoReducer,
  SettingsReducer,
  HistoryReducer,
  OptionsReducer
})
