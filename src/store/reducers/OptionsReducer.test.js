import * as actions from '../actions/OptionsAction'
import OptionsReducer from './OptionsReducer'

test('OptionsReducer return default state on init', () => {
  const expectState = {
    isActive: false,
    error: ''
  }
  expect(OptionsReducer(undefined, {})).toEqual(expectState)
})

test('OptionsReducer return toggled state after trigger OptionsToggleSuccessAction', () => {
  const initState = {
    isActive: false,
    error: ''
  }
  const expectState = {
    isActive: true,
    error: ''
  }
  expect(
    OptionsReducer(initState, actions.OptionsToggleSuccessAction())
  ).toEqual(expectState)
})

test('OptionsReducer return error state after trigger OptionsToggleErrorAction', () => {
  const message = 'Some error'
  const initState = {
    isActive: false,
    error: ''
  }
  const expectState = {
    isActive: false,
    error: message
  }
  expect(
    OptionsReducer(initState, actions.OptionsToggleErrorAction(message))
  ).toEqual(expectState)
})
