import * as history from '../actions/HistoryAction'
import { initState } from '../main/initState'

const init = initState.HistoryReducer

export default function HistoryReducer (state = init, action) {
  switch (action.type) {
    case history.HISTORY_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error
      }
    }

    case history.HISTORY_GET_LOADING: {
      return {
        ...state,
        loading: true,
        error: ''
      }
    }

    case history.HISTORY_GET_SUCCESS: {
      return {
        ...state,
        list: action.list,
        loading: false
      }
    }

    case history.HISTORY_REMOVE_SUCCESS: {
      return {
        ...state,
        list: state.list.filter(item => item.id !== action.id),
        loading: false,
        error: ''
      }
    }

    case history.HISTORY_RESET_SUCCESS: {
      return {
        ...state,
        list: [],
        loading: false,
        error: ''
      }
    }
  }

  return state
}
