import * as todo from '../actions/TodoAction'
import TodoReducer from './TodoReducer'

test('TodoReducer return default state on init', () => {
  const expectState = {
    loading: false,
    list: [],
    error: '',
    text: ''
  }
  expect(TodoReducer(undefined, {})).toEqual(expectState)
})

test('TodoReducer return error state after trigger TodoErrorAction', () => {
  const expectState = {
    loading: false,
    list: [],
    error: 'Error message',
    text: ''
  }
  expect(TodoReducer(undefined, todo.TodoErrorAction('Error message'))).toEqual(
    expectState
  )
})

test('TodoReducer return loading state after trigger TodoGetLoadingAction', () => {
  const expectState = {
    loading: true,
    list: [],
    error: '',
    text: ''
  }
  expect(TodoReducer(undefined, todo.TodoGetLoadingAction())).toEqual(
    expectState
  )
})

test('TodoReducer return success state after trigger TodoGetSuccessAction', () => {
  const list = []

  const expectState = {
    loading: false,
    list: list,
    error: '',
    text: ''
  }
  expect(TodoReducer(undefined, todo.TodoGetSuccessAction(list))).toEqual(
    expectState
  )
})

test('TodoReducer return updated list after trigger TodoAddSuccessAction', () => {
  const item = {}

  const expectState = {
    loading: false,
    list: [item],
    error: '',
    text: ''
  }
  expect(TodoReducer(undefined, todo.TodoAddSuccessAction(item))).toEqual(
    expectState
  )
})

test('TodoReducer return shorted list after trigger TodoRemoveSuccessAction', () => {
  const id = 1

  const initState = {
    loading: false,
    list: [
      {
        id: 1,
        name: 'Eva'
      },
      {
        id: 2,
        name: 'Natasha'
      }
    ],
    error: '',
    text: ''
  }

  const expectState = {
    loading: false,
    list: [
      {
        id: 2,
        name: 'Natasha'
      }
    ],
    error: '',
    text: ''
  }
  expect(TodoReducer(initState, todo.TodoRemoveSuccessAction(id))).toEqual(
    expectState
  )
})

test('TodoReducer return default state after trigger TodoResetSuccessAction', () => {
  const expectState = {
    loading: false,
    list: [],
    error: '',
    text: ''
  }
  expect(TodoReducer(undefined, todo.TodoResetSuccessAction())).toEqual(
    expectState
  )
})

test('TodoReducer return correct text after trigger TodoTextGetSuccessAction', () => {
  const text = ''

  const expectState = {
    loading: false,
    list: [],
    error: '',
    text: text
  }
  expect(TodoReducer(undefined, todo.TodoTextGetSuccessAction())).toEqual(
    expectState
  )
})

test('TodoReducer correct update text after trigger TodoTextUpdateSuccessAction', () => {
  const text = 'Some text'

  const expectState = {
    loading: false,
    list: [],
    error: '',
    text: text
  }
  expect(
    TodoReducer(undefined, todo.TodoTextUpdateSuccessAction(text))
  ).toEqual(expectState)
})

test('TodoReducer reset error after trigger TodoTextUpdateAction', () => {
  const text = ''

  const initState = {
    loading: false,
    list: [],
    error: 'Some error',
    text: text
  }

  const expectState = {
    loading: false,
    list: [],
    error: '',
    text: text
  }
  expect(TodoReducer(initState, todo.TodoTextUpdateSuccessAction())).toEqual(
    expectState
  )
})
