import * as options from '../actions/OptionsAction'
import { initState } from '../main/initState'

const init = initState.OptionsReducer

export default function OptionsReducer (state = init, action) {
  switch (action.type) {
    case options.OPTIONS_TOGGLE_ERROR: {
      return {
        ...state,
        error: action.error
      }
    }

    case options.OPTIONS_TOGGLE_SUCCESS: {
      return {
        ...state,
        isActive: !state.isActive,
        error: ''
      }
    }
  }

  return state
}
