import * as todo from '../actions/SettingsAction'
import { initState } from '../main/initState'

const init = initState.SettingsReducer

export default function SettingsReducer (state = init, action) {
  switch (action.type) {
    case todo.SETTINGS_ERROR: {
      return {
        ...state,
        error: action.error
      }
    }

    case todo.SETTINGS_GET_SUCCESS: {
      return {
        ...state,
        ...action.settings
      }
    }

    case todo.SETTINGS_UPDATE_SUCCESS: {
      return {
        ...state,
        ...action.settings
      }
    }
  }

  return state
}
