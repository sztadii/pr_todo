import * as history from '../actions/HistoryAction'
import HistoryReducer from './HistoryReducer'

test('HistoryReducer returns default state on init', () => {
  const expectState = {
    loading: false,
    list: [],
    error: ''
  }
  expect(HistoryReducer(undefined, {})).toEqual(expectState)
})

test('HistoryReducer returns error state after trigger HistoryErrorAction', () => {
  const expectState = {
    loading: false,
    list: [],
    error: 'Error message'
  }
  expect(
    HistoryReducer(undefined, history.HistoryErrorAction('Error message'))
  ).toEqual(expectState)
})

test('HistoryReducer returns loading state after trigger HistoryGetLoadingAction', () => {
  const expectState = {
    loading: true,
    list: [],
    error: ''
  }
  expect(HistoryReducer(undefined, history.HistoryGetLoadingAction())).toEqual(
    expectState
  )
})

test('HistoryReducer returns success state after trigger HistoryGetSuccessAction', () => {
  const list = []

  const expectState = {
    loading: false,
    list: list,
    error: ''
  }
  expect(
    HistoryReducer(undefined, history.HistoryGetSuccessAction(list))
  ).toEqual(expectState)
})

test('HistoryReducer returns shorted list after trigger HistoryRemoveSuccessAction', () => {
  const id = 1

  const initState = {
    loading: false,
    list: [
      {
        id: 1,
        name: 'Eva'
      },
      {
        id: 2,
        name: 'Natasha'
      }
    ],
    error: ''
  }

  const expectState = {
    loading: false,
    list: [
      {
        id: 2,
        name: 'Natasha'
      }
    ],
    error: ''
  }
  expect(
    HistoryReducer(initState, history.HistoryRemoveSuccessAction(id))
  ).toEqual(expectState)
})

test('HistoryReducer returns empty list after trigger HistoryResetSuccessAction', () => {
  const expectState = {
    loading: false,
    list: [],
    error: ''
  }
  expect(
    HistoryReducer(undefined, history.HistoryResetSuccessAction())
  ).toEqual(expectState)
})
