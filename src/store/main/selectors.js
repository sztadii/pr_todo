export function historyLoading (state) {
  return state.HistoryReducer.loading
}
export function historyList (state) {
  return state.HistoryReducer.list
}
export function historyError (state) {
  return state.HistoryReducer.error
}

export function optionsIsActive (state) {
  return state.OptionsReducer.isActive
}
export function optionsError (state) {
  return state.OptionsReducer.error
}

export function settingsLang (state) {
  return state.SettingsReducer.lang
}
export function settingsError (state) {
  return state.SettingsReducer.error
}

export function todoLoading (state) {
  return state.TodoReducer.loading
}
export function todoList (state) {
  return state.TodoReducer.list
}
export function todoError (state) {
  return state.TodoReducer.error
}
export function todoText (state) {
  return state.TodoReducer.text
}
