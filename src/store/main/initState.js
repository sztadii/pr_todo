/* eslint-disable import/prefer-default-export */
export const initState = {
  HistoryReducer: {
    loading: false,
    list: [],
    error: ''
  },
  OptionsReducer: {
    isActive: false,
    error: ''
  },
  SettingsReducer: {
    lang: 'en',
    error: ''
  },
  TodoReducer: {
    loading: false,
    list: [],
    error: '',
    text: ''
  }
}
