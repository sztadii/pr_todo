import * as selectors from './selectors'
import { initState } from './initState'

test('HistoryList returns value of history list', () => {
  expect(selectors.historyList(initState)).toEqual([])
})

test('HistoryLoading returns value of history loading', () => {
  expect(selectors.historyLoading(initState)).toEqual(false)
})

test('HistoryError returns value of history error', () => {
  expect(selectors.historyError(initState)).toEqual('')
})

test('OptionsIsActive returns value of options active state', () => {
  expect(selectors.optionsIsActive(initState)).toEqual(false)
})

test('OptionsError returns value of options error', () => {
  expect(selectors.optionsError(initState)).toEqual('')
})

test('SettingsLang returns value of settings lang', () => {
  expect(selectors.settingsLang(initState)).toEqual('en')
})

test('SettingsError returns value of settings error', () => {
  expect(selectors.settingsError(initState)).toEqual('')
})

test('TodoList returns value of todo list', () => {
  expect(selectors.todoList(initState)).toEqual([])
})

test('TodoLoading returns value of todo loading', () => {
  expect(selectors.todoLoading(initState)).toEqual(false)
})

test('TodoText returns value of todo text', () => {
  expect(selectors.todoText(initState)).toEqual('')
})
