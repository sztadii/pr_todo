export const HISTORY_ERROR = 'HISTORY_ERROR'
export function HistoryErrorAction (error) {
  return {
    type: HISTORY_ERROR,
    error
  }
}

export const HISTORY_GET = 'HISTORY_GET'
export function HistoryGetAction () {
  return {
    type: HISTORY_GET
  }
}

export const HISTORY_GET_LOADING = 'HISTORY_GET_LOADING'
export function HistoryGetLoadingAction () {
  return {
    type: HISTORY_GET_LOADING
  }
}

export const HISTORY_GET_SUCCESS = 'HISTORY_GET_SUCCESS'
export function HistoryGetSuccessAction (list) {
  return {
    type: HISTORY_GET_SUCCESS,
    list
  }
}

export const HISTORY_REMOVE = 'HISTORY_REMOVE'
export function HistoryRemoveAction (id) {
  return {
    type: HISTORY_REMOVE,
    id
  }
}

export const HISTORY_REMOVE_SUCCESS = 'HISTORY_REMOVE_SUCCESS'
export function HistoryRemoveSuccessAction (id) {
  return {
    type: HISTORY_REMOVE_SUCCESS,
    id
  }
}

export const HISTORY_RESET = 'HISTORY_RESET'
export function HistoryResetAction () {
  return {
    type: HISTORY_RESET
  }
}

export const HISTORY_RESET_SUCCESS = 'HISTORY_RESET_SUCCESS'
export function HistoryResetSuccessAction () {
  return {
    type: HISTORY_RESET_SUCCESS
  }
}
