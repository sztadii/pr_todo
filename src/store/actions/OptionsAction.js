export const OPTIONS_TOGGLE = 'OPTIONS_TOGGLE'
export function OptionsToggleAction () {
  return {
    type: OPTIONS_TOGGLE
  }
}

export const OPTIONS_TOGGLE_SUCCESS = 'OPTIONS_TOGGLE_SUCCESS'
export function OptionsToggleSuccessAction () {
  return {
    type: OPTIONS_TOGGLE_SUCCESS
  }
}

export const OPTIONS_TOGGLE_ERROR = 'OPTIONS_TOGGLE_ERROR'
export function OptionsToggleErrorAction (error) {
  return {
    type: OPTIONS_TOGGLE_ERROR,
    error
  }
}
