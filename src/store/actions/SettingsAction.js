export const SETTINGS_ERROR = 'SETTINGS_ERROR'
export function SettingsErrorAction (error) {
  return {
    type: SETTINGS_ERROR,
    error
  }
}

export const SETTINGS_GET = 'SETTINGS_GET'
export function SettingsGetAction () {
  return {
    type: SETTINGS_GET
  }
}

export const SETTINGS_GET_SUCCESS = 'SETTINGS_GET_SUCCESS'
export function SettingsGetSuccessAction (settings) {
  return {
    type: SETTINGS_GET_SUCCESS,
    settings
  }
}

export const SETTINGS_UPDATE = 'SETTINGS_UPDATE'
export function SettingsUpdateAction (settings) {
  return {
    type: SETTINGS_UPDATE,
    settings
  }
}

export const SETTINGS_UPDATE_SUCCESS = 'SETTINGS_UPDATE_SUCCESS'
export function SettingsUpdateSuccessAction (settings) {
  return {
    type: SETTINGS_UPDATE_SUCCESS,
    settings
  }
}
