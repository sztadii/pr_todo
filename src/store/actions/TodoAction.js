export const TODO_ERROR = 'TODO_ERROR'
export function TodoErrorAction (error) {
  return {
    type: TODO_ERROR,
    error
  }
}

export const TODO_GET = 'TODO_GET'
export function TodoGetAction () {
  return {
    type: TODO_GET
  }
}

export const TODO_GET_LOADING = 'TODO_GET_LOADING'
export function TodoGetLoadingAction () {
  return {
    type: TODO_GET_LOADING
  }
}

export const TODO_GET_SUCCESS = 'TODO_GET_SUCCESS'
export function TodoGetSuccessAction (list) {
  return {
    type: TODO_GET_SUCCESS,
    list
  }
}

export const TODO_ADD = 'TODO_ADD'
export function TodoAddAction (item) {
  return {
    type: TODO_ADD,
    item
  }
}

export const TODO_ADD_SUCCESS = 'TODO_ADD_SUCCESS'
export function TodoAddSuccessAction (item) {
  return {
    type: TODO_ADD_SUCCESS,
    item
  }
}

export const TODO_REMOVE = 'TODO_REMOVE'
export function TodoRemoveAction (id) {
  return {
    type: TODO_REMOVE,
    id
  }
}

export const TODO_REMOVE_SUCCESS = 'TODO_REMOVE_SUCCESS'
export function TodoRemoveSuccessAction (id) {
  return {
    type: TODO_REMOVE_SUCCESS,
    id
  }
}

export const TODO_RESET = 'TODO_RESET'
export function TodoResetAction () {
  return {
    type: TODO_RESET
  }
}

export const TODO_RESET_SUCCESS = 'TODO_RESET_SUCCESS'
export function TodoResetSuccessAction () {
  return {
    type: TODO_RESET_SUCCESS
  }
}

export const TODO_TEXT_GET = 'TODO_TEXT_GET'
export function TodoTextGetAction () {
  return {
    type: TODO_TEXT_GET
  }
}

export const TODO_TEXT_GET_SUCCESS = 'TODO_TEXT_GET_SUCCESS'
export function TodoTextGetSuccessAction (text = '') {
  return {
    type: TODO_TEXT_GET_SUCCESS,
    text
  }
}

export const TODO_TEXT_UPDATE = 'TODO_TEXT_UPDATE'
export function TodoTextUpdateAction (text = '') {
  return {
    type: TODO_TEXT_UPDATE,
    text
  }
}

export const TODO_TEXT_UPDATE_SUCCESS = 'TODO_TEXT_UPDATE_SUCCESS'
export function TodoTextUpdateSuccessAction (text = '') {
  return {
    type: TODO_TEXT_UPDATE_SUCCESS,
    text
  }
}
