export const TEXT_MIN = 5
export const TEXT_MAX = 30
export const LIST_MAX = 10

export const DEFAULT_LANGUAGES = [
  {
    value: 'en',
    name: 'english'
  },
  {
    value: 'pl',
    name: 'polish'
  },
  {
    value: 'de',
    name: 'german'
  },
  {
    value: 'es',
    name: 'spanish'
  }
]

export const DEFAULT_COLORS = [
  {
    name: 'white',
    value: '#ffffff'
  },
  {
    name: 'gray',
    value: '#f5f5f5'
  },
  {
    name: 'gray-dark',
    value: '#c2c2c2'
  },
  {
    name: 'black',
    value: '#4d4d4d'
  },
  {
    name: 'red',
    value: '#AF2F2F'
  }
]

export const COLORS = [
  '#ffffff',
  '#f5f5f5',
  '#c2c2c2',
  '#AF2F2F',
  '#f44336',
  '#e91e63',
  '#9c27b0',
  '#673ab7',
  '#3f51b5',
  '#2196f3',
  '#03a9f4',
  '#00bcd4',
  '#009688',
  '#4caf50',
  '#8bc34a',
  '#cddc39',
  '#ffeb3b',
  '#ffc107',
  '#ff9800',
  '#ff5722',
  '#795548',
  '#607d8b'
]
